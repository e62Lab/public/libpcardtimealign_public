package si.ijs.e6;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import si.ijs.e6.pcard.PcardTimeAlignment;
import si.ijs.e6.pcard.TimestampPair;

/**
 * Created by matjaz on 10/30/18.
 */
public class PcardTimeAlignmentTest {
    class TsPair implements TimestampPair {
        long timestamp, counter;

        public TsPair(TsPair other) {
            timestamp = other.getTimestamp();
            counter = other.getCounter();
        }

        public TsPair(long t, long c) {
            timestamp = t;
            counter = c;
        }

        @Override
        public long getTimestamp() {
            return timestamp;
        }

        @Override
        public long getCounter() {
            return counter;
        }

        @Override
        public void setTimestamp(long value) {
            timestamp = value;
        }

        @Override
        public void setCounter(long value) {
            counter = value;
        }
    }

    ArrayList<TsPair> listOfTestData;
    long deltaT;
    boolean debugLog = true;

    @org.junit.Before
    public void setUp() throws Exception {
        // create some test data with frequency 1000 Hz
        deltaT = 1000000L;      // value is in nano seconds, this translates to 1 milli second
        // create 100 samples (timestamp & counter pairs) and assign values to them
        listOfTestData = new ArrayList<>(100);
        for (int i = 0; i < 100; ++i)
            listOfTestData.add(new TsPair(i*deltaT*14, (i*14) % 1024));

        if (debugLog)
            System.out.printf("Created 'input': %d samples with frequency of %4.1f Hz\n", listOfTestData.size(), 1e9/deltaT);

        // disable logger for these tests
        if (!debugLog)
            Logger.getLogger(PcardTimeAlignment.class.getName()).setLevel(Level.WARNING);
    }

    /**
     * This test implements a minimal basic scenario for the use of constant frequency time alignment
     * @throws Exception
     */
    @org.junit.Test
    public void constantFrequencyAlignment() throws Exception {
        // create timeAlign algorithm and register a copy of test data with it (thus leaving input data unmodified)
        PcardTimeAlignment<TsPair> timeAlign = new PcardTimeAlignment<>();
        // have some mismatch in frequency to make alignment actually do some work
        // note that frequency seems to be set in GHz; this is because timestamps were in nano seconds
        // but the algorithm works in absolute numbers and does not know anything about units.
        double alignToFrequency = 1.2 / deltaT;
        timeAlign.setupConstantFrequency(alignToFrequency);

        if (debugLog)
            System.out.printf("Initialized procedure for aligning to %4.1f Hz\n", 1e9*alignToFrequency);

        int bufSize = 100;
        timeAlign.setBufferSize(bufSize);

        if (debugLog)
            System.out.printf("Setting align procedure buffer to %d\n", bufSize);
        for (int i = 0; i < listOfTestData.size(); ++i)
            timeAlign.registerPacket(new TsPair(listOfTestData.get(i)));

        // flush and process output
        timeAlign.stop();
        PcardTimeAlignment.DataCollection<TsPair> outData = timeAlign.getOutputData();
        if (debugLog) {
            System.out.printf("In:  %d:%d %d:%d .. %d:%d \n", listOfTestData.get(0).getTimestamp(), listOfTestData.get(0).getCounter(), listOfTestData.get(1).getTimestamp(), listOfTestData.get(1).getCounter(), listOfTestData.get(99).getTimestamp(), listOfTestData.get(99).getCounter());
            System.out.printf("Out: %d:%d %d:%d .. %d:%d \n", outData.get(0).getTimestamp(), outData.get(0).getCounter(), outData.get(1).getTimestamp(), outData.get(1).getCounter(), outData.get(99).getTimestamp(), outData.get(99).getCounter());
        }
    }
}