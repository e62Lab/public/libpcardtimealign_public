package si.ijs.e6.pcard;

/**
 * Created by matjaz on 10/30/18.
 *
 * Interface to use in time alignment, to provide counters and timestamps for the algorithm to process.
 * Timestamp is the time on the reference device (device that receives and processes data)
 * Counter is counter of samples, which is equivalent to timestamp in arbitrary time units on the sampling device
 */
public interface TimestampPair {
    long getTimestamp();
    long getCounter();
    void setTimestamp(long value);
    void setCounter(long value);

}
