package si.ijs.e6.pcard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by matjaz on 4/18/18.
 * Helper for aligning PCARD wireless body sensor data in time. Can be used to stream in the data from sensor and
 * get a stream of time-aligned data. It supports multiple algorithms for various time alignment methods.
 *
 * Background:
 * PCARD sensors are streaming data that is not well aligned in time, but contains metadata that can be used to align
 * it in postprocessing: counters (that correspond to time on the remote end) and timestamps (time when each individual
 * packet of data was received on the receiving end).
 * PCARD normal data stream consists of 10-bit counters in every packet of 14 10-bit data samples. Since 10-bits
 * overflow frequently when sampling with e.g. 128 Hz, reconnects occur (and are not marked in old files), and
 * reconnects break the counter sequences, time alignment is not trivial.
 *
 * Example of use:
 *  while (ls.readLines(callback, false)) {
 *      if (callback.streamPacket()) {
 *          ... // process stream packet in a callback and pass it to PcardTimeAlignment instance
 *          pcardTimeAlignment.registerPacket(timestamp, sampleCounter);
 *      }
 *  }
 *  pcardTimeAlignment.stop();
 *  // now fetch data from timeAlign.getOutputData()
 *
 *  Details:
 *  inputDataCache and outputDataCache members are used to hold data before and after processing, respectively. Note
 *  that processing a single block might not produce any output, because one block should remain in input cache to be
 *  processed again (some mis-alignments can only be discovered by the next block). InternalDataCache is used to hold
 *  the data that were already somewhat processed but might require re-aligning.
 *  TODO: implement the above.
 *
 *
 * TODO:
 *  a whole block may be shifted in time, which may be discovered only later, when processing the next block
 *  (e.g. long delays on both ends of the block may be misinterpreted as disconnects, making a part of otherwise
 *  connected data seem like own block), the next block will then be aligned correctly but will overlap with the
 *  already processed heavily misaligned block. This old block should now be somehow fixed in 'postmortem'.
 *      - Keep each processed block in cache until a new block is processed (for now, each block is flushed as soon as
 *          it is processed in full)
 *      - If the newly processed blocks indicates an offset in old block, that one is processed before being flushed
 *      - Only short blocks may be kept in cache (e.g. less than half or quarter size of the 'buffer size')
 *      - A block may get another measure - min and max offset in time - when it is complete; the default could then be
 *          to shift it to minimum offset (or counter-based offset), especially if it is a small block
 */
public class PcardTimeAlignment<TP extends TimestampPair> {
    // invalid counter may mean a disconnect occurred
    public static final long INVALID_COUNTER = -66666; //Long.MIN_VALUE;
    public static final long INVALID_TIMESTAMP = -1L;

    private int cacheSize = 100;

    /**
     * A structure that stores basic information about blocks
     * Block is a part within the measurement that contains continuous sampled signal. Disconnects and long pauses
     *  slice the measurement into multiple blocks.
     * Note: variables representing counters are stored extended (more than 10 bits) but not fixed
     */
    static class BlockInfo {
        // what interval of data does this block encompass
        long startingCounter = -1;
        long endingCounter;

        // the interval of this block in real-time
        long startingTimestamp;
        long endingTimestamp;

        // is the end of the block a marked stop (disconnect / end of measurement) or an unmarked one
        boolean markedStop;
        /**
         * when converting from counter-based times back to timestamps, one takes reference timestamp (likely based
         * on very first timestamp of the block) and counters to derive first approximation for timestamps. This
         * first approximation is usually physically unrealistic and minimalTimestampOffset is the measure of how
         * unrealistic it is (timestamp approximations should have this offset subtracted).
         */
        long minimalTimestampOffset;
        // when block is started it may start with initial increase in counter (if starting packet is offset in
        // time a lot)
        long initialCounterOffset = 0;
        // mark blocks as flushed when they are removed from input data collection
        boolean flushed = false;
    }

    /**
     * DataCollection is a container for caching input and output (aligned) data
     *
     * More than one block may be stored in DataCollection and Blocks may span multiple DataCollections.
     * DataCollection is not a fixed-size container but is mostly used with predefined target size.
     *
     * DataCollection only works with pairs of counter/timestamp and knows nothing about actual data. The number and
     * indices of pairs will remain the same throughout the alignment process though. Therefore, data can be managed
     * safely by the calling class.
     */
    public static class DataCollection<TP extends TimestampPair> implements Iterable<TP> {
        List<TP> list;
        int targetSize = 100;   // some reasonable default

        /**
         * Create a new data collection of the predefined size
         * @param expectedSize    the predefined size of the collection
         */
        DataCollection(int expectedSize) {
            targetSize = expectedSize;
            // create a list with the proper initial capacity (size will be 0 though!)
            list = new ArrayList<>(expectedSize);
        }

        /**
         * This constructor could be used by the splice method - it creates a view of an interval of the collection.
         * A collection will be created, which will hold references to target collection. Modifications of this
         * collection will be reflected in the target collection.
         *
         * @param parent    target (or source) collection whose elements should be referenced
         * @param i1        index of the first element
         * @param i2        index of the 'end' element (one past the last stored element index)
         */
        private DataCollection(DataCollection<TP> parent, int i1, int i2) {
            targetSize = i1 < i2 ? i2-i1 : 0;
            list = parent.list.subList(i1, i2);
        }

        /**
         * Splice collection (use a range of indices from this collection to create a new linked collection)
         * @param b    block that supplies the counters that define the range for splice
         * @return a view on an interval within this collection
         */
        public DataCollectionView<TP> splice(BlockInfo b) {
            // define indices of the interval within the collection
            int i1 = -1;
            int i2 = -1;
            // search backwards, since this function will mostly be used for accessing the recently processed blocks
            for (int i = list.size()-1; i > 0; --i) {
                if (list.get(i).getCounter() < b.endingCounter) {
                    i2 = i;
                    break;
                }
            }
            for (int i = i2; i > 0; --i) {
                if (list.get(i).getCounter() < b.startingCounter) {
                    i1 = i+1;
                    break;
                }
            }
            return new DataCollectionView<TP>(this, i1 >= 0 ? i1 : 0, i2 >= 0 ? i2 : 0);
        }

        /**
         * Query for the predefined size of this collection (not its current number of contained elements)
         * @return the predefined size (target size) of this collection.
         */
        public int targetSize() {
            return targetSize;
        }

        /**
         * Returns the index where new data will be added
         * Position can serve (with combination of size) as the indicator of the collection fullness.
         * @return current size of the collection, also the index of where new elements will be added.
         */
        public int size() {
            return list.size();
        }

        /**
         * Check whether the collection is already full (size equals predefined size)
         * @return true if the collection is full
         */
        public boolean full() {
            return targetSize() == size();
        }

        /**
         * After a cache has been used to synchronize a data interval, the interval can be shifted - part of it will
         * be removed from cache, part of it will shift to the front of cache; new interval of data can then be added
         * to the cache
         * @param shiftAmount specifies by how much (how many elements) should the contents be shifted
         */
        void shiftData(int shiftAmount) {
            int s = Math.min(shiftAmount, list.size());
            //int e = list.size()-1 - s;
            //list = list.subList(s, e);
            list.subList(0, s).clear();
        }

        /**
         * Add a single datum pair (a point in time) to the collection (will resize the collection if necessary)
         * @param datum pair of (timestamp, counter)
         */
        void addDatumPoint(TP datum) {
            list.add(datum);
        }

        /**
         * Remove the datum that was added last (it is located in list's final position)
         */
        void removeLastDatum() {
            list.remove(list.size()-1);
        }

        /**
         * Append another data points collection at the end of this collection (merge collections)
         * Note: only append properly time sorted collections, the time intervals should not interleave.
         * @param morePoints    the collection to append
         */
        void append(DataCollection<TP> morePoints) {
            list.addAll(morePoints.list);
        }

        /**
         * Get i-th element in the collection; this function is not range-checked
         * @param i    index of the element
         * @return Value of the element (the datum pair)
         */
        public TP get(int i) {
            return list.get(i);
        }

        /**
         * To enable foreach use, the following iterator is implemented (TODO: check for correctness of code)
         * @return the iterator instance
         */
        @Override
        public Iterator<TP> iterator() {
            return new Iterator<TP>() {
                int pos = 0;

                @Override
                public boolean hasNext() {
                    return pos < list.size();
                }

                @Override
                public TP next() {
                    return list.get(pos++);
                }
            };
        }

        /**
         * Empty the container.
         */
        public void clear() {
            list.clear();
        }
    }

    /**
     * A view on the DataCollection (kind of a pointer to interval within the parent collection)
     * @param <TP> type of data items within the collection
     */
    public static class DataCollectionView<TP extends TimestampPair> extends DataCollection<TP> {
        /**
         * This constructor is used by the splice method - it creates a view of an interval of the collection.
         * A collection will be created, which will hold references to target collection. Modifications of this
         * collection will be reflected in the target collection.
         *
         * @param parent    target (or source) collection whose elements should be referenced
         * @param i1        index of the first element
         * @param i2        index of the 'end' element (one past the last stored element index)
         */
        private DataCollectionView(DataCollection<TP> parent, int i1, int i2) {
            super(i2-i1);
            targetSize = i1 < i2 ? i2-i1 : 0;
            list = parent.list.subList(i1, i2);
            // disable modifications to structure.
        }

        private static void doNotAllowStructureModifications() {
            throw new RuntimeException("Error: cannot modify structure of a DataCollectionView.");
        }

        // disable the following overloaded functions:
        void shiftData(int shiftAmount) {
            doNotAllowStructureModifications();
        }
        void removeLastDatum() {
            doNotAllowStructureModifications();
        }
        void append(DataCollection morePoints) {
            doNotAllowStructureModifications();
        }

        // but enable formatting of parent collection through these functions

        /**
         * Clear this collection (and the corresponding subset of the parent collection)
         */
        public void clear() {
            list.clear();
        }
    }

    /**
     * The interface for time alignment algorithms to implement to be used in the alignment procedure.
     *
     * This interface is used only to allow several different algorithms for alignment.
     * Several algorithms are envisioned:
     *  - basic, frequency - based: assume the declared frequency and stick to it
     *  - dynamic: determine the frequency dynamically, each time interval may have different sampling frequency
     *  - dynamic, multi-measurement: as dynamic but using multiple concurrent measurements as input [optional]
     */
    interface TimeAlignAlgorithmInterface<TP extends TimestampPair>  {
        /**
         * Tell the method that 'silent' disconnects (that is, disconnects that are not recognised by the external
         * logic, and are thus not registered through @link registerDisconnect) are possible.
         * Note: MobECG will not record disconnects up until revision 1.8.0
         * @param enabled false (default) if all disconnects will be registered, true otherwise
         */
        void enableSilentDisconnectsDetection(boolean enabled);

        /**
         * Main functionality of the interface - time-aligning the data;
         * The function *must* align some part of the input collection but *may* leave some data for later alignment
         * (e.g. if not enough data is available).
         *
         * Note: input is unaligned/raw; output must be fully aligned
         *
         * @param input    input data collection (corresponding timestamps & counters); the function must
         *                 clear out the data it already aligned and leave the rest untouched
         * @param cache    cache data collection: to be used by the algorithm to cache data pairs, might require
         *                 further processing
         * return output data collection - time-aligned data that must contain all valida data from input
         *                 collection and nothing more
         */
        DataCollection<TP> alignData(DataCollection<TP> input, DataCollection<TP> cache);

        /**
         * finis will be called to notify the algorithm instance that the process has finished, no
         * more data samples will be registered, and all the data in the cachces and the
         * pipeline should be flushed to output
         */
        void finish();
    }

    /**
     * Setup the size of buffer; the while processing, the buffer will be filled up before trying to align its elements.
     * This function should be called in initialization phase and can have undefined consequences if called
     *  while the processing is in progress.
     * Note: the terminology is both buffer and cache, used interchangeably.
     * @param bufferSize the number of packets to keep in a buffer before aligning their times
     */
    public void setBufferSize(int bufferSize) {
        cacheSize = bufferSize;
        inputDataCache = new DataCollection<TP>(cacheSize);
    }

    // the instance of alignment algorithm that is used internally
    private TimeAlignAlgorithmInterface<TP> timeAlignInstance = null;
    // the input data is gathered in this cache before it is sent to alignment
    private DataCollection<TP> inputDataCache;
    // some of the aligned data is gathered in this cache for possible re-alignment, before it is sent to output
    private DataCollection<TP> internalCache = new DataCollection<TP>(100); //TODO make the allocated space dynamic
    // the processed data is stored in output cache, and can be accessed and regularly cleared from the outside
    private DataCollection<TP> outputDataCache;

    /**
     * Setup time alignment using a constant frequency method
     * @param fs the sampling frequency
     */
    public void setupConstantFrequency(double fs) {
        // TimeAlignSimpleRealTimeAlgorithm is a method that implements time alignment using constant frequency, that is,
        // it uses counters only to derive the timestamps of the input data
        TimeAlignSimpleRealTimeAlgorithm<TP> ta = new TimeAlignSimpleRealTimeAlgorithm<TP>();
        timeAlignInstance = ta;
        ta.constantFs = fs;
        ta.constantSamplingTime = 1/fs;
    }

    /**
     * Get the output data (time-aligned); not every packet sent into alignment is immediately available here.
     * The output data is cached by the PcardAlignment class and may be read at any time. Use
     * {@link #processAndFlushOutput} method to ensure some of the input data is flushed to output.
     *
     * @return A collection of output data. There is no restraints on how it should be handled.
     */
    public DataCollection<TP> getOutputData() {
        return outputDataCache == null ? new DataCollection<>(0) : outputDataCache;
    }

    /**
     * Process active cache and flush one block of the cache to the output.
     * A single block (defined as set of data that seems to be continuously sampled, where it is up to the selected time
     * alignment algorithm to decide on specific criteria for that) is processed and flushed. Note that cache might
     * contain multiple blocks, the number of which is not known in advance. This method will process at least on block.
     *
     * @return the number of data points that were flushed
     */
    private int processAndFlushOutput() {
        DataCollection<TP> outputArray = timeAlignInstance.alignData(inputDataCache, internalCache);

        // renew the working dataset;
        // note: the old cache will remain in memory (time align method might still be using it)
        //setupCachingSize(inputDataCache.targetSize());
        if (outputDataCache == null) {
            outputDataCache = outputArray;
        } else {
            outputDataCache.append(outputArray);
        }

        // return the new output size
        return outputArray.size();
    }

    /**
     * Tells the method that a disconnect has occurred some time after the past registered event.
     * Note that calling this method will necessarily end current block of data in cache and will therefore call
     * {@link #processAndFlushOutput} method
     * @param data data instance that will be modified to signal a disconnect; getTimestamp() should return the
     *             (approximate) time of the disconnect or INVALID_TIMESTAMP if the time is unknown.
     */
    public void registerDisconnect(TP data) {
        if ((inputDataCache != null) && (inputDataCache.size() > 0)) {
            data.setCounter(INVALID_COUNTER);
            inputDataCache.addDatumPoint(data);
            processAndFlushOutput();
        }

        // disconnect used to be marked by element with invalid counter
    }

    /**
     * Register a packet with the given sample counter
     * @param data timestamp data point, containing valid counter and timestamp
     *
     * @return true if packet caused the cache to flush to output
     */
    public boolean registerPacket(TP data) {
        if (inputDataCache == null) {
            inputDataCache = new DataCollection<>(cacheSize);
        }

        boolean processed = false;
        if (inputDataCache.full()) {
            processAndFlushOutput();
            processed = true;
        }

        inputDataCache.addDatumPoint(data);
        return processed;
    }

    /**
     * Tells the method to stop processing, that the input stream has ended.
     * This will trigger processing and flushing of the data currently still in cache and will call the finalize
     * method of the alignment algorithm (which might do additional alignment).
     */
    public void stop() {
        if (inputDataCache != null) {
            //registerDisconnect(INVALID_TIMESTAMP);
            // anything left to flush?

            while (inputDataCache.size() > 0)
                processAndFlushOutput();
        }
        if (timeAlignInstance != null) {
            timeAlignInstance.finish();
        }
    }

    /**
     * (re)initialize time alignment instance (only required after stop has been called).
     * After calling this funciton, time alignment instance  is reset into initial state and ne
     * alignment can be used to perform new alignment..
     */
    public void init() {
        inputDataCache = null;
        internalCache.clear();
        // do not clear output cache, since this is user's data and responsibility
        outputDataCache = null;
    }
}
